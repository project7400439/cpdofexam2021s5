package com.agiletestingalliance;

public class AboutCPDOF{

  public String desc() {
    String str = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: \n 1. Completely hands on. \n 2. 100% Lab/Tools Driven \n 3. Covers all the tools in entire lifecycle \n 4. You will not only learn but experience the entire DevOps lifecycle. \n 5. Practical Assessment to help you solidify your learnings.";
    return str;
  
  }
  
  }
